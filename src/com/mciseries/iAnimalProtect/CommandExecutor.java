package com.mciseries.iAnimalProtect;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.mciseries.iAnimalProtect.files.Messages;
import com.mciseries.iAnimalProtect.handlers.*;

public class CommandExecutor implements org.bukkit.command.CommandExecutor {
	public CommandExecutor(iAnimalProtect p) {}
	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2,
			String[] arg3) {
		FileConfiguration config = Bukkit.getPluginManager().getPlugin("iAnimalProtect").getConfig();
		if(arg3.length == 0 || !(arg0 instanceof Player)) {
			arg0.sendMessage(new Messages().getError("IncorrectUsage", false));
		}
		else {
			if(arg3[0].equalsIgnoreCase("add"))
				new Add(config, arg0, arg1, arg3);
			else if(arg3[0].equalsIgnoreCase("remove"))
				new Remove(config, arg0, arg1, arg3);
			else if(arg3[0].equalsIgnoreCase("list"))
				new List(config, arg0, arg1, arg3);
			else if(arg3[0].equalsIgnoreCase("open"))
				new Open(config, arg0, arg1, arg3);
			else if(arg3[0].equalsIgnoreCase("close"))
				new Close(config, arg0, arg1, arg3);
			else
				arg0.sendMessage(new Messages().getError("IncorrectUsage", false));
		}
		return true;
	}
	
}
