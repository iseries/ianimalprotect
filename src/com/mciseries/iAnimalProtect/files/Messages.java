package com.mciseries.iAnimalProtect.files;

import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Messages {
	File file = new File("plugins/iAnimalProtect/messages.yml");
	FileConfiguration conf = YamlConfiguration.loadConfiguration(file);
	public Messages() {
		
	}
	public void saveDefaults() throws IOException {
		if(!file.exists()) {
			file.createNewFile();
			conf.set("AddedPlayer", "Player <player> is now allowed to kill here");
			conf.save(file);
			conf.set("RemovedPlayer", "Player <player> is no longer allowed to kill here");
			conf.save(file);
			conf.set("ListPlayers", "Players allowed to kill here: <players>");
			conf.save(file);
			conf.set("Errors.NotEnoughArgs", "Not enough arguments");
			conf.save(file);
			conf.set("Errors.TooManyArgs", "Too many arguments");
			conf.save(file);
			conf.set("Errors.NoPlot", "There is no plot here");
			conf.save(file);
			conf.set("Errors.NotYourPlot", "You don't own this plot");
			conf.save(file);
			conf.set("Errors.CannotKill", "You cannot kill animals here");
			conf.save(file);
			conf.set("Errors.IncorrectUsage", "Usage: /ap <add/remove/list> [player]");
			conf.save(file);
			conf.set("Errors.NoneAdded", "Nobody is allowed to kill on this plot");
			conf.save(file);
			conf.set("Errors.CannotShear", "You cannot shear animals here");
			conf.save(file);
			conf.set("OpenedPlot", "This plot is now able to be killed on by anyone");
			conf.save(file);
			conf.set("ClosedPlot", "This plot is no longer able to be killed on by anyone");
			conf.save(file);
		}
	}
	public String get(String message, String replace, String replaceWith) {
		return ChatColor.AQUA + "[iAnimalProtect] " + ChatColor.RESET + conf.getString(message).replace(replace, replaceWith);
	}
	public String getSuccess(String message, String replace, String replaceWith) {
		return ChatColor.AQUA + "[iAnimalProtect] " + ChatColor.GREEN + conf.getString(message).replace(replace, replaceWith);
	}
	public String getError(String message, boolean strong) {
		if(strong)
			return ChatColor.AQUA + "[iAnimalProtect] " + ChatColor.DARK_RED + conf.getString("Errors." + message);
		else
			return ChatColor.AQUA + "[iAnimalProtect] " + ChatColor.RED + conf.getString("Errors." + message);
	}
}
