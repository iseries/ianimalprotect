package com.mciseries.iAnimalProtect;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import org.bukkit.plugin.java.JavaPlugin;

import com.mciseries.iAnimalProtect.files.Messages;

public class iAnimalProtect extends JavaPlugin {
	public void onEnable() {
		File file = new File("plugins/iAnimalProtect/config.yml");
		saveDefaultConfig();
		try {
			Scanner scanner = new Scanner(file);
			while(scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if(line.contains("#")) {
					file.delete();
					saveDefaultConfig();
				}
			}
		} catch (FileNotFoundException e) {}
		try {
			new Messages().saveDefaults();
		} catch (IOException e) {
			getLogger().warning("(iAP) Couldn't save config.yml! " + e.toString());
		}
		getCommand("ap").setExecutor(new CommandExecutor(this));
		getServer().getPluginManager().registerEvents(new DamageListener(this), this);
	}
}
