package com.mciseries.iAnimalProtect.handlers;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.mciseries.iAnimalProtect.files.Messages;

import com.worldcretornica.plotme.Plot;
import com.worldcretornica.plotme.PlotManager;

public class Remove {
	Messages msg = new Messages();
	File file = new File("plugins/iAnimalProtect/config.yml");
	Logger log = Logger.getLogger("Minecraft");
	public Remove(FileConfiguration conf, CommandSender cs, Command c, String[] a) {
		Player s = Bukkit.getPlayer(cs.getName());
		if(a.length < 2) {
			s.sendMessage(msg.getError("NotEnoughArgs", false));
		}
		else if(a.length > 2) {
			s.sendMessage(msg.getError("TooManyArgs", false));
		}
		else {
			String id = PlotManager.getPlotId(s.getLocation());
			Plot p = PlotManager.getPlotById(s, id);
			if(id.equalsIgnoreCase("")) {
				s.sendMessage(msg.getError("NoPlot", false));
			}
			else if(PlotManager.isPlotAvailable(id, s) || (!p.owner.equalsIgnoreCase(s.getName()) && !s.hasPermission("ap.admin"))) {
				s.sendMessage(msg.getError("NotYourPlot", false));
			}
			else if(p.owner.equalsIgnoreCase(s.getName()) || s.hasPermission("ap.admin")) {
				if(conf.getConfigurationSection("Plots." + s.getWorld().getName() + "." + id).getKeys(false).size() == 1)
					conf.set("Plots." + s.getWorld().getName() + "." + id, null);
				else
					conf.set("Plots." + s.getWorld().getName() + "." + id + "." + a[1], null);
				try {
					conf.save(file);
				} catch (IOException e) {
					log.warning("(iAP) Couldn't save config.yml! " + e.toString());
				}
				String name = a[1];
				s.sendMessage(msg.getSuccess("RemovedPlayer", "<player>", name));
			}
		}
	}
}