package com.mciseries.iAnimalProtect.handlers;

import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.mciseries.iAnimalProtect.files.Messages;

import com.worldcretornica.plotme.Plot;
import com.worldcretornica.plotme.PlotManager;

public class List {
	Messages msg = new Messages();
	public List(FileConfiguration conf, CommandSender cs, Command c, String[] a) {
		Player s = Bukkit.getPlayer(cs.getName());
		if(a.length < 1) {
			s.sendMessage(msg.getError("NotEnoughArgs", false));
		}
		else if(a.length > 1) {
			s.sendMessage(msg.getError("TooManyArgs", false));
		}
		else {
			String id = PlotManager.getPlotId(s.getLocation());
			Plot p = PlotManager.getPlotById(s, id);
			if(id.equalsIgnoreCase("")) {
				s.sendMessage(msg.getError("NoPlot", false));
			}
			else if(PlotManager.isPlotAvailable(id, s) || (!p.owner.equalsIgnoreCase(s.getName()) && !s.hasPermission("ap.admin"))) {
				s.sendMessage(msg.getError("NotYourPlot", false));
			}
			else if(p.owner.equalsIgnoreCase(s.getName()) || s.hasPermission("ap.admin")) {
				StringBuilder sb = new StringBuilder("");
				if(conf.getConfigurationSection("Plots." + s.getWorld().getName() + "." + id) == null)
					s.sendMessage(msg.getError("NoneAdded", false));
				else {
					Set<String> added = conf.getConfigurationSection("Plots." + s.getWorld().getName() + "." + id).getKeys(false);
					for(String name : added) {
						if(conf.getBoolean("Plots." + s.getWorld().getName() + "." + id + "." + name)) {
							sb.append(name + ", ");
						}
					}
					String list = sb.toString().substring(0, sb.toString().length() - 2);
					list = list + ".";
					s.sendMessage(msg.get("ListPlayers", "<players>", list));
				}
			}
		}
	}
}
