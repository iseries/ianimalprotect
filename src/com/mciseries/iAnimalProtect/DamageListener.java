package com.mciseries.iAnimalProtect;

import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerShearEntityEvent;
import org.bukkit.entity.*;

import com.mciseries.iAnimalProtect.files.Messages;
import com.worldcretornica.plotme.Plot;
import com.worldcretornica.plotme.PlotManager;

public class DamageListener implements Listener {
	private Messages msg = new Messages();
	private iAnimalProtect plugin;
	public DamageListener(iAnimalProtect plugin) {
		this.plugin = plugin;
	}
	@EventHandler
	public void onEntityDamage(EntityDamageEvent evt) {
		if(evt instanceof EntityDamageByEntityEvent) {
			if(PlotManager.isPlotWorld(evt.getEntity().getLocation()) && !plugin.getConfig().getList("UnprotectedWorlds").contains(evt.getEntity().getWorld().getName())) {
				EntityDamageByEntityEvent e = (EntityDamageByEntityEvent)evt;
				Entity damager = e.getDamager();
				Entity damagee = e.getEntity();
				if(damager instanceof Explosive || damager instanceof Projectile) {
					e.setCancelled(true); evt.setCancelled(true);
				}
				if(damager instanceof Player && (damagee instanceof Animals || damagee instanceof Squid || damagee instanceof Bat || damagee instanceof Golem || damagee instanceof Snowman) && !((Player)damager).hasPermission("ap.admin")) {
					String id = PlotManager.getPlotId(damagee.getLocation());
					Plot p = PlotManager.getPlotById((Player)damager, id);
					if(!plugin.getConfig().getBoolean("Plots." + damager.getWorld().getName() + "." + id + "." + ((Player)damager).getName()) && !p.owner.equalsIgnoreCase(((Player)damager).getName()) && !((Player)damager).hasPermission("ap.admin") && !plugin.getConfig().getBoolean("Plots." + damager.getWorld().getName() + "." + id + ".Open")) {
						e.setCancelled(true); evt.setCancelled(true);
						((Player)damager).sendMessage(msg.getError("CannotKill", true));
					}
				}
			}
		}
	}
	@EventHandler
	public void onPlayerShearEntityEvent(PlayerShearEntityEvent evt) {
			if(PlotManager.isPlotWorld(evt.getEntity().getLocation()) && !plugin.getConfig().getList("UnprotectedWorlds").contains(evt.getEntity().getWorld().getName())) {
				Entity damager = (Entity) evt.getPlayer();
				Entity damagee = evt.getEntity();
				String id = PlotManager.getPlotId(damagee.getLocation());
				Plot p = PlotManager.getPlotById((Player)damager, id);
				if(!plugin.getConfig().getBoolean("Plots." + damager.getWorld().getName() + "." + id + "." + ((Player)damager).getName()) && !p.owner.equalsIgnoreCase(((Player)damager).getName()) && !((Player)damager).hasPermission("ap.admin") && !plugin.getConfig().getBoolean("Plots." + damager.getWorld().getName() + "." + id + ".Open") && damagee instanceof Sheep) {
					evt.setCancelled(true);
					((Player)damager).sendMessage(msg.getError("CannotShear", true));
				}
			}
	}
}
